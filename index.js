const puppeteer = require('puppeteer')
const listOfWebsites = require('./list')
let browser =  null;
let name = "John Due"
let message = "Hello this is my message for you!"
let email = "test@gmail.com"
let captchaClient = require('./captcha')
let x = 0;



async function autoScroll(page) {

    await page.evaluate(async () => {
        await new Promise((resolve, reject) => {
            var totalHeight = 0;
            var distance = 100;
            var timer = setInterval(() => {
                var scrollHeight = document.body.scrollHeight;
                window.scrollBy(0, distance);
                totalHeight += distance;
  
                if(totalHeight >= scrollHeight - window.innerHeight){
                    clearInterval(timer);
                    resolve();
                }
            }, 400);
        })
    })
  
  }

(async () => {
  try {
    browser = await puppeteer.launch({
        headless: false,
        defaultViewport: null,
        args:['--start-maximized'],
    })
    const page = (await browser.pages())[0]
    let submitButton = null;
    let foundEmail = false
    for (let website of  listOfWebsites) {
        try {
            await page.goto("https://www.publinews.gt/gt/contacto", { waitUntil: "networkidle0", timeout: 120000})
            await autoScroll(page)
            await page.waitForSelector('form', {timeout: 300000})
            
            try {
                await page.waitForSelector('textarea')
                const [messageTextbox] = await page.$x('//textarea')
                await page.evaluate((el, message ) => { el.value = message }, messageTextbox, message)
            } catch (e) {
                console.log('did not find a textarea')
            }
            
            try {
                const allInputs = await page.$$('input')
                let i = 0
                while( i < allInputs.length ) {
                    const inputAttr = await page.evaluate(el => el.getAttribute("type"), allInputs[i])
                    console.log(inputAttr)
                    if (inputAttr == 'search') {
                        await page.evaluate((el, name )=>{ el.value = name }, allInputs[i], name)
                    } else if (inputAttr == 'text') {
                        
                        if (foundEmail) {
                            await page.evaluate((el, name )=>{ el.value = name }, allInputs[i], name)
                        } else {
                            await page.evaluate((el, name )=>{ el.value = name }, allInputs[i], email)
                        }
                        
                    } else if (inputAttr == 'email') {
                        foundEmail = true
                        await page.evaluate((el, email )=>{ el.value = email }, allInputs[i], email)
                    } else if (inputAttr == 'checkbox') { 
                        try {
                            await page.evaluate(el => el.click(), allInputs[i])
                        } catch (e) {
                        }
                    } else if (inputAttr == 'submit') {
                        submitButton = allInputs[i]
                    }
                    i++
                }
            } catch (e) {
                console.log(e)
            }

            try {
                const allSelectFields = await page.$$('select')
                let x = 0
                while( x < allSelectFields.length ) { 
                    await allSelectFields[x].type('USA')
                    x++
                }
            } catch (e) {
                console.log(e)
            }
            
            try {
                const siteKeyV2 = await page.evaluate(el => el.getAttribute("data-sitekey"), await page.$('div[data-sitekey]'))
                console.log(siteKeyV2)
                if (siteKeyV2) {

                 let response = await captchaClient.decodeRecaptchaV2({
                     googlekey: siteKeyV2,
                     pageurl: website
                 })
                 console.log(response.text)
                    await page.evaluate( (el, token) => el.innerHTML = token, await page.$('textarea[id="g-recaptcha-response"]'), response.text)
                    try {
                        await page.waitForTimeout(5000)
                        await submitButton.click()
                        console.log('submited form with captcha...')
                        await page.waitForNavigation({timeout: 120000})
                    } catch (e) {
                        console.log(e)
                        console.log('failed to submit form')
                    }
                } else {
                    try {
                        await page.waitForTimeout(5000)
                        await submitButton.click()
                        console.log('submited form with no captcha...')
                        await page.waitForNavigation({timeout: 120000})
                    } catch (e) {
                        console.log(e)
                        console.log('failed to submit form')
                    }
                }
             } catch (e) {
                 console.log(e)
             }
        } catch (e) {
            console.log(e)
            console.log('Failed to submit contact page...')
        }
        await page.waitForTimeout(5000)
        x++
    }

    await page.waitForTimeout(100000)
    await browser.close()
  } catch (e) {
      throw e
  }
 
})()